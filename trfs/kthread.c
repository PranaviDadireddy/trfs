#include "trfs.h"

struct trfs_open_req {
	struct file *filp;
	long offset;
	void *record;
	struct completion done;
	struct list_head kthread_ctl_list;
};

static struct trfs_kthread_ctl {
#define TRFS_KTHREAD_ZOMBIE 0x00000001
	u32 flags;
	struct mutex mux;
	struct list_head req_list;
	wait_queue_head_t wait;
} trfs_kthread_ctl;
static struct task_struct *trfs_kthread;

/**
 * trfs_threadfn
 * @ignored: ignored
 *
 * The trfs kernel thread that has the responsibility of writing
 * traced records to tfile
 *
 * Returns zero on success; non-zero otherwise
 */
static int trfs_threadfn(void *ignored)
{
	int err = 0;
	mm_segment_t curr_fs;

	set_freezable();
	while (1)  {
		struct trfs_open_req *req;

		wait_event_freezable(
				trfs_kthread_ctl.wait,
				(!list_empty(&trfs_kthread_ctl.req_list)
				 || kthread_should_stop()));
		mutex_lock(&trfs_kthread_ctl.mux);
		if (trfs_kthread_ctl.flags & TRFS_KTHREAD_ZOMBIE) {
			mutex_unlock(&trfs_kthread_ctl.mux);
			goto out;
		}
		while (!list_empty(&trfs_kthread_ctl.req_list)) {
			req = list_first_entry(&trfs_kthread_ctl.req_list,
					struct trfs_open_req,
					kthread_ctl_list);
			list_del(&req->kthread_ctl_list);

			/* do vfs_write */
			curr_fs = get_fs();
			set_fs(KERNEL_DS);
			err = vfs_write(req->filp, req->record,
					req->offset, &req->filp->f_pos);

			set_fs(curr_fs);
			kfree(req->record);
			complete(&req->done);
		}
		mutex_unlock(&trfs_kthread_ctl.mux);
	}
out:
	return err;
}

/* Initialise wait queue and start kthread */
int trfs_init_kthread(void)
{
	int rc = 0;

	mutex_init(&trfs_kthread_ctl.mux);
	init_waitqueue_head(&trfs_kthread_ctl.wait);
	INIT_LIST_HEAD(&trfs_kthread_ctl.req_list);
	trfs_kthread = kthread_run(&trfs_threadfn, NULL,
			"trfs-kthread");
	if (IS_ERR(trfs_kthread)) {
		rc = PTR_ERR(trfs_kthread);
		printk(KERN_ERR "%s: Failed to create kernel thread;"
				"rc = [%d]\n", __func__, rc);
	}
	return rc;
}

/* destroy kthread */
void trfs_destroy_kthread(void)
{
	struct trfs_open_req *req, *tmp;

	mutex_lock(&trfs_kthread_ctl.mux);
	trfs_kthread_ctl.flags |= TRFS_KTHREAD_ZOMBIE;
	list_for_each_entry_safe(req, tmp, &trfs_kthread_ctl.req_list,
			kthread_ctl_list) {
		list_del(&req->kthread_ctl_list);
		complete(&req->done);
	}
	mutex_unlock(&trfs_kthread_ctl.mux);
	kthread_stop(trfs_kthread);
	wake_up(&trfs_kthread_ctl.wait);
}

/**
 * trfs_write_to_file
 * @filp: file pointer of tfile
 * @rec: pointer to record which is to be written to tfile
 * @offset: length of record to be written
 *
 * This function puts the record to be written to tfile in queue.
 *
 * Returns zero on success; non-zero otherwise
 */
int trfs_write_to_file(struct file *filp, void *rec, long offset)
{
	struct trfs_open_req req;
	int rc = 0;

	init_completion(&req.done);
	req.record = rec;
	req.filp = filp;
	req.offset = offset;

	mutex_lock(&trfs_kthread_ctl.mux);
	if (trfs_kthread_ctl.flags & TRFS_KTHREAD_ZOMBIE) {
		rc = -EIO;
		mutex_unlock(&trfs_kthread_ctl.mux);
		printk(KERN_ERR "%s: We are in the middle of shutting down",
				__func__);
		goto out;
	}
	list_add_tail(&req.kthread_ctl_list, &trfs_kthread_ctl.req_list);
	mutex_unlock(&trfs_kthread_ctl.mux);
	wake_up(&trfs_kthread_ctl.wait);
	wait_for_completion(&req.done);
out:
	return rc;
}


