/*
   e Copyright (c) 1998-2015 Erez Zadok
 * Copyright (c) 2009	   Shrikar Archak
 * Copyright (c) 2003-2015 Stony Brook University
 * Copyright (c) 2003-2015 The Research Foundation of SUNY
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "trfs.h"
#include "crypto.h"

static ssize_t trfs_read(struct file *file, char __user *buf,
		size_t count, loff_t *ppos)
{
	int err = 0;
	struct record_read *rec = NULL;
	char *buf_pathname = NULL;
	struct file *filp;
	unsigned long *bitmap;
	char *pathname = NULL;
	char dest_md5[MD5_DIGEST_SIZE];
	char *dest_buff = NULL;

	struct file *lower_file;
	struct dentry *dentry = file->f_path.dentry;

	lower_file = trfs_lower_file(file);
	err = vfs_read(lower_file, buf, count, ppos);
	/* update our inode atime upon a successful lower read */
	if (err >= 0)
		fsstack_copy_attr_atime(d_inode(dentry),
				file_inode(lower_file));

	/* tracing read function */
	bitmap = trfs_bitmap(dentry->d_inode->i_sb);
	if (!bitmap) {
		printk(KERN_ERR "null reference to bitmap\n");
		goto out;
	}
	if (*bitmap & MASK_READ) {
		filp = trfs_tfile(dentry->d_inode->i_sb);

		buf_pathname = kmalloc(MAX_PATH_LEN, GFP_KERNEL);
		if (!buf_pathname) {
			printk(KERN_ERR "malloc failed\n");
			goto out;
		}

		pathname = dentry_path_raw(file->f_path.dentry,
				buf_pathname, MAX_PATH_LEN);
		if (IS_ERR(pathname)) {
			printk(KERN_ERR "Error while getting dentry path errno - %ld\n",
					PTR_ERR(pathname));
			goto out;
		}

		rec = kmalloc(sizeof(struct record_read)
				+ strlen(pathname), GFP_KERNEL);
		if (!rec) {
			printk(KERN_ERR "malloc failed\n");
			goto out;
		}
		/* fill the record */
		spin_lock(&recId_lock);
		record_id = record_id + 1;
		rec->id = record_id;
		spin_unlock(&recId_lock);

		rec->ret_val = err;
		rec->type = TYPE_READ;
		rec->flags = file->f_flags;
		rec->mode = file->f_mode;
		rec->pid = current->pid;
		rec->file_ptr = (long int)file;

		strcpy(&rec->pathname[0], pathname);
		rec->len_pathname = strlen(pathname);

		err = trfs_calculate_md5(dest_md5, buf, strlen(buf));
		dest_buff = kmalloc(2 * MD5_DIGEST_SIZE + 1, GFP_KERNEL);
		if (!dest_buff) {
			printk(KERN_ERR "malloc failed\n");
			goto out;
		}
		trfs_to_hex(dest_buff, dest_md5, 16);
		rec->buf_len = strlen(dest_buff);
		rec->size = sizeof(struct record_read) + rec->len_pathname;
		/* write to tfile */
		trfs_write_to_file(filp, rec, rec->size);
		trfs_write_to_file(filp, dest_buff, strlen(dest_buff));
	}
out:
	kfree(buf_pathname);
	return err;
}


static ssize_t trfs_write(struct file *file, const char __user *buf,
		size_t count, loff_t *ppos)
{
	int err = 0;
	struct record_write *rec = NULL;
	char *buf_pathname = NULL;
	struct file *filp;
	unsigned long *bitmap;
	char *pathname = NULL;
	struct file *lower_file;
	struct dentry *dentry = file->f_path.dentry;
	char *file_data = NULL;
	int file_len = 0;

	lower_file = trfs_lower_file(file);
	err = vfs_write(lower_file, buf, count, ppos);
	/* update our inode times+sizes upon a successful lower write */
	if (err >= 0) {
		fsstack_copy_inode_size(d_inode(dentry),
				file_inode(lower_file));
		fsstack_copy_attr_times(d_inode(dentry),
				file_inode(lower_file));
	}

	/* tracing write function */
	bitmap = trfs_bitmap(dentry->d_inode->i_sb);
	if (!bitmap) {
		printk("null reference to bitmap\n");
		goto out;
	}
	if (*bitmap & MASK_WRITE) {
		/* get tfile and allocate memory for record*/
		filp = trfs_tfile(dentry->d_inode->i_sb);

		buf_pathname = kmalloc(MAX_PATH_LEN, GFP_KERNEL);
		if (!buf_pathname) {
			printk(KERN_ERR "malloc failed\n");
			goto out;
		}

		pathname = dentry_path_raw(file->f_path.dentry,
				buf_pathname, MAX_PATH_LEN);

		if (IS_ERR(pathname)) {
			printk("Error while getting dentry path errno - %ld\n",
					PTR_ERR(pathname));
			goto out;
		}

		rec = kmalloc(sizeof(struct record_write)
				+ strlen(pathname), GFP_KERNEL);
		if (!rec) {
			printk(KERN_ERR "malloc failed\n");
			goto out;
		}
		/* fill the record */
		spin_lock(&recId_lock);
		record_id = record_id + 1;
		rec->id = record_id;
		spin_unlock(&recId_lock);

		rec->ret_val = err;
		rec->type = TYPE_WRITE;
		rec->flags = file->f_flags;
		rec->mode = file->f_mode;
		rec->pid = current->pid;
		rec->file_ptr = (long int)file;

		strcpy(&rec->pathname[0], pathname);
		rec->len_pathname = strlen(pathname);
		file_len = strlen(buf);
		rec->buf_len = file_len;
		/* copy file data into a temporary buffer */
		file_data = kmalloc(MAX_BUF_LEN, GFP_KERNEL);
		if (!file_data) {
			printk(KERN_ERR "malloc failed\n");
			goto out;
		}
		memcpy(file_data, buf, file_len);

		rec->size = sizeof(struct record_write) + rec->len_pathname;
		/* write to tfile */
		trfs_write_to_file(filp, rec, rec->size);
		trfs_write_to_file(filp, file_data, file_len);
	}
out:
	kfree(buf_pathname);
	return err;
}

static int trfs_readdir(struct file *file, struct dir_context *ctx)
{
	int err;
	struct file *lower_file = NULL;
	struct dentry *dentry = file->f_path.dentry;

	lower_file = trfs_lower_file(file);
	err = iterate_dir(lower_file, ctx);
	file->f_pos = lower_file->f_pos;
	if (err >= 0)		/* copy the atime */
		fsstack_copy_attr_atime(d_inode(dentry),
				file_inode(lower_file));
	return err;
}

static long trfs_unlocked_ioctl(struct file *file, unsigned int cmd,
		unsigned long arg)
{
	long err = -ENOTTY;
	struct file *lower_file;
	unsigned long *bitmap;
	extern DECLARE_BITMAP(fs_bitmap, MAX_BITS);

	lower_file = trfs_lower_file(file);
	bitmap = trfs_bitmap(lower_file->f_inode->i_sb);

	switch (cmd) {
	case SET_BITMAP:
		if (copy_from_user(fs_bitmap, (void __user *)arg,
					BITS_TO_BYTES(MAX_BITS)))
			return -EFAULT;
		bitmap = fs_bitmap;
		err = 0;
		break;
	case GET_BITMAP:
		if (copy_to_user((void __user *)arg, fs_bitmap,
					BITS_TO_BYTES(MAX_BITS)))
			return -EFAULT;

		err = 0;
		break;
	default:
		/* XXX: use vfs_ioctl if/when VFS exports it */
		if (!lower_file || !lower_file->f_op)
			goto out;
		if (lower_file->f_op->unlocked_ioctl)
			err = lower_file->f_op->unlocked_ioctl(lower_file, cmd, arg);

		/* some ioctls can change inode attributes (EXT2_IOC_SETFLAGS) */
		if (!err)
			fsstack_copy_attr_all(file_inode(file),
					file_inode(lower_file));
	}
out:
	return err;
}

#ifdef CONFIG_COMPAT
static long trfs_compat_ioctl(struct file *file, unsigned int cmd,
		unsigned long arg)
{
	long err = -ENOTTY;
	struct file *lower_file;

	lower_file = trfs_lower_file(file);

	/* XXX: use vfs_ioctl if/when VFS exports it */
	if (!lower_file || !lower_file->f_op)
		goto out;
	if (lower_file->f_op->compat_ioctl)
		err = lower_file->f_op->compat_ioctl(lower_file, cmd, arg);

out:
	return err;
}
#endif

static int trfs_mmap(struct file *file, struct vm_area_struct *vma)
{
	int err = 0;
	bool willwrite;
	struct file *lower_file;
	const struct vm_operations_struct *saved_vm_ops = NULL;

	/* this might be deferred to mmap's writepage */
	willwrite = ((vma->vm_flags | VM_SHARED | VM_WRITE) == vma->vm_flags);

	/*
	 * File systems which do not implement ->writepage may use
	 * generic_file_readonly_mmap as their ->mmap op.  If you call
	 * generic_file_readonly_mmap with VM_WRITE, you'd get an -EINVAL.
	 * But we cannot call the lower ->mmap op, so we can't tell that
	 * writeable mappings won't work.  Therefore, our only choice is to
	 * check if the lower file system supports the ->writepage, and if
	 * not, return EINVAL (the same error that
	 * generic_file_readonly_mmap returns in that case).
	 */
	lower_file = trfs_lower_file(file);
	if (willwrite && !lower_file->f_mapping->a_ops->writepage) {
		err = -EINVAL;
		printk(KERN_ERR "trfs: lower file system does not "
				"support writeable mmap\n");
		goto out;
	}

	/*
	 * find and save lower vm_ops.
	 *
	 * XXX: the VFS should have a cleaner way of finding the lower vm_ops
	 */
	if (!TRFS_F(file)->lower_vm_ops) {
		err = lower_file->f_op->mmap(lower_file, vma);
		if (err) {
			printk(KERN_ERR "trfs: lower mmap failed %d\n", err);
			goto out;
		}
		saved_vm_ops = vma->vm_ops; /* save: came from lower ->mmap */
	}

	/*
	 * Next 3 lines are all I need from generic_file_mmap.  I definitely
	 * don't want its test for ->readpage which returns -ENOEXEC.
	 */
	file_accessed(file);
	vma->vm_ops = &trfs_vm_ops;

	file->f_mapping->a_ops = &trfs_aops; /* set our aops */
	if (!TRFS_F(file)->lower_vm_ops) /* save for our ->fault */
		TRFS_F(file)->lower_vm_ops = saved_vm_ops;

out:
	return err;
}

static int trfs_open(struct inode *inode, struct file *file)
{
	int err = 0;
	struct file *lower_file = NULL;
	struct path lower_path;
	struct record_open *rec = NULL;
	char *buf = NULL;
	struct file *filp;
	unsigned long *bitmap;
	char *pathname = NULL;

	/* don't open unhashed/deleted files */
	if (d_unhashed(file->f_path.dentry)) {
		err = -ENOENT;
		goto out_err;
	}

	file->private_data =
		kzalloc(sizeof(struct trfs_file_info), GFP_KERNEL);
	if (!TRFS_F(file)) {
		err = -ENOMEM;
		goto out_err;
	}

	/* open lower object and link trfs's file struct to lower's */
	trfs_get_lower_path(file->f_path.dentry, &lower_path);
	lower_file = dentry_open(&lower_path, file->f_flags, current_cred());
	path_put(&lower_path);
	if (IS_ERR(lower_file)) {
		err = PTR_ERR(lower_file);
		lower_file = trfs_lower_file(file);
		if (lower_file) {
			trfs_set_lower_file(file, NULL);
			fput(lower_file); /* fput calls dput for lower_dentry */
		}
	} else {
		trfs_set_lower_file(file, lower_file);
	}

	if (err)
		kfree(TRFS_F(file));
	else
		fsstack_copy_attr_all(inode, trfs_lower_inode(inode));
out_err:
	bitmap = trfs_bitmap(inode->i_sb);
	if (!bitmap) {
		printk("null reference to bitmap\n");
		goto return_err;
	}
	if (*bitmap & MASK_OPEN) {
		/* get tfile and allocate memory for record*/
		filp = trfs_tfile(inode->i_sb);

		buf = kmalloc(MAX_PATH_LEN, GFP_KERNEL);
		if (!buf) {
			printk(KERN_ERR "malloc failed\n");
			goto return_err;
		}

		pathname = dentry_path_raw(file->f_path.dentry, buf, MAX_PATH_LEN);
		if (IS_ERR(pathname)) {
			printk("Error while getting dentry path errno - %ld\n",
					PTR_ERR(pathname));
			goto return_err;
		}

		rec = kmalloc(sizeof(struct record_open) + strlen(pathname), GFP_KERNEL);
		if (!rec) {
			printk(KERN_ERR "malloc failed\n");
			goto return_err;
		}

		/* fill the record */
		spin_lock(&recId_lock);
		record_id = record_id + 1;
		rec->id = record_id;
		spin_unlock(&recId_lock);

		rec->ret_val = err;
		rec->type = TYPE_OPEN;
		rec->flags = file->f_flags;
		rec->mode = file->f_mode;
		rec->pid = current->pid;
		rec->file_ptr = (long int)file;

		strcpy(&rec->pathname[0], pathname);
		rec->len_pathname = strlen(pathname);
		rec->size = sizeof(struct record_open) + rec->len_pathname;
		/* write to tfile */
		trfs_write_to_file(filp, rec, rec->size);
	}
return_err:
	kfree(buf);
	return err;
}

static int trfs_flush(struct file *file, fl_owner_t id)
{

	int err = 0;
	struct record_flush *rec = NULL;
	char *buf = NULL;
	struct file *filp;
	unsigned long *bitmap;
	char *pathname = NULL;


	struct file *lower_file = NULL;

	lower_file = trfs_lower_file(file);
	if (lower_file && lower_file->f_op && lower_file->f_op->flush) {
		filemap_write_and_wait(file->f_mapping);
		err = lower_file->f_op->flush(lower_file, id);
	}

	bitmap = trfs_bitmap(file->f_inode->i_sb);
	if (!bitmap) {
		printk("null reference to bitmap\n");
		goto return_err;
	}
	printk("bitmap %ld, %ld, %ld \n",(*bitmap & MASK_FLUSH),*bitmap, MASK_FLUSH);

	if (*bitmap & MASK_FLUSH) {
		/* get tfile and allocate memory for record*/
		filp = trfs_tfile(file->f_inode->i_sb);

		buf = kmalloc(MAX_PATH_LEN, GFP_KERNEL);
		if (!buf) {
			printk(KERN_ERR "malloc failed\n");
			goto return_err;
		}

		pathname = dentry_path_raw(file->f_path.dentry, buf, MAX_PATH_LEN);
		if (IS_ERR(pathname)) {
			printk("Error while getting dentry path errno - %ld\n",
					PTR_ERR(pathname));
			goto return_err;
		}

		rec = kmalloc(sizeof(struct record_flush) + strlen(pathname), GFP_KERNEL);
		if (!rec) {
			printk(KERN_ERR "malloc failed\n");
			goto return_err;
		}
		/*fill the record*/
		spin_lock(&recId_lock);
		record_id = record_id + 1;
		rec->id = record_id;
		spin_unlock(&recId_lock);

		rec->ret_val = err;
		rec->type = TYPE_FLUSH;
		rec->pid = current->pid;
		rec->file_ptr = (long int)file;

		strcpy(&rec->pathname[0], pathname);
		rec->len_pathname = strlen(pathname);
		rec->size = sizeof(struct record_flush) + rec->len_pathname;
		/* write to tfile */
		trfs_write_to_file(filp, rec, rec->size);
	}
return_err:
	kfree(buf);
	return err;
}

/* release all lower object references & free the file info structure */
static int trfs_file_release(struct inode *inode, struct file *file)
{
	struct file *lower_file;

	int err = 0;
	struct record_release *rec = NULL;
	char *buf = NULL;
	struct file *filp;
	unsigned long *bitmap;
	char *pathname = NULL;



	lower_file = trfs_lower_file(file);
	if (lower_file) {
		trfs_set_lower_file(file, NULL);
		fput(lower_file);
	}

	/*tracing flush function*/
	bitmap = trfs_bitmap(inode->i_sb);
	if (!bitmap) {
		printk("null reference to bitmap\n");
		goto return_err;
	}
	if (*bitmap & MASK_RELEASE) {
		/* get tfile and allocate memory for record*/
		filp = trfs_tfile(inode->i_sb);

		buf = kmalloc(MAX_PATH_LEN, GFP_KERNEL);
		if (!buf) {
			printk(KERN_ERR "malloc failed\n");
			goto return_err;
		}

		pathname = dentry_path_raw(file->f_path.dentry, buf, MAX_PATH_LEN);
		if (IS_ERR(pathname)) {
			printk("Error while getting dentry path errno - %ld\n",
					PTR_ERR(pathname));
			goto return_err;
		}

		rec = kmalloc(sizeof(struct record_release) + strlen(pathname), GFP_KERNEL);
		if (!rec) {
			printk(KERN_ERR "malloc failed\n");
			goto return_err;
		}
		/*fill the record*/
		spin_lock(&recId_lock);
		record_id = record_id + 1;
		rec->id = record_id;
		spin_unlock(&recId_lock);

		rec->ret_val = err;
		rec->type = TYPE_RELEASE;
		rec->pid = current->pid;
		rec->file_ptr = (long int)file;

		strcpy(&rec->pathname[0], pathname);
		rec->len_pathname = strlen(pathname);
		rec->size = sizeof(struct record_release) + rec->len_pathname;
		/* write to tfile */
		trfs_write_to_file(filp, rec, rec->size);
	}
return_err:
	kfree(buf);
	kfree(TRFS_F(file));
	return err;
}

static int trfs_fsync(struct file *file, loff_t start, loff_t end,
		int datasync)
{
	int err;
	struct file *lower_file;
	struct path lower_path;
	struct dentry *dentry = file->f_path.dentry;

	err = __generic_file_fsync(file, start, end, datasync);
	if (err)
		goto out;
	lower_file = trfs_lower_file(file);
	trfs_get_lower_path(dentry, &lower_path);
	err = vfs_fsync_range(lower_file, start, end, datasync);
	trfs_put_lower_path(dentry, &lower_path);
out:
	return err;
}

static int trfs_fasync(int fd, struct file *file, int flag)
{
	int err = 0;
	struct file *lower_file = NULL;

	lower_file = trfs_lower_file(file);
	if (lower_file->f_op && lower_file->f_op->fasync)
		err = lower_file->f_op->fasync(fd, lower_file, flag);

	return err;
}

/*
 * Trfs cannot use generic_file_llseek as ->llseek, because it would
 * only set the offset of the upper file.  So we have to implement our
 * own method to set both the upper and lower file offsets
 * consistently.
 */
static loff_t trfs_file_llseek(struct file *file, loff_t offset, int whence)
{
	int err;
	struct file *lower_file;

	err = generic_file_llseek(file, offset, whence);
	if (err < 0)
		goto out;

	lower_file = trfs_lower_file(file);
	err = generic_file_llseek(lower_file, offset, whence);

out:
	return err;
}

/*
 * Trfs read_iter, redirect modified iocb to lower read_iter
 */
	ssize_t
trfs_read_iter(struct kiocb *iocb, struct iov_iter *iter)
{
	int err;
	struct file *file = iocb->ki_filp, *lower_file;

	lower_file = trfs_lower_file(file);
	if (!lower_file->f_op->read_iter) {
		err = -EINVAL;
		goto out;
	}

	get_file(lower_file); /* prevent lower_file from being released */
	iocb->ki_filp = lower_file;
	err = lower_file->f_op->read_iter(iocb, iter);
	iocb->ki_filp = file;
	fput(lower_file);
	/* update upper inode atime as needed */
	if (err >= 0 || err == -EIOCBQUEUED)
		fsstack_copy_attr_atime(d_inode(file->f_path.dentry),
				file_inode(lower_file));
out:
	return err;
}

/*
 * Trfs write_iter, redirect modified iocb to lower write_iter
 */
	ssize_t
trfs_write_iter(struct kiocb *iocb, struct iov_iter *iter)
{
	int err;
	struct file *file = iocb->ki_filp, *lower_file;

	lower_file = trfs_lower_file(file);
	if (!lower_file->f_op->write_iter) {
		err = -EINVAL;
		goto out;
	}

	get_file(lower_file); /* prevent lower_file from being released */
	iocb->ki_filp = lower_file;
	err = lower_file->f_op->write_iter(iocb, iter);
	iocb->ki_filp = file;
	fput(lower_file);
	/* update upper inode times/sizes as needed */
	if (err >= 0 || err == -EIOCBQUEUED) {
		fsstack_copy_inode_size(d_inode(file->f_path.dentry),
				file_inode(lower_file));
		fsstack_copy_attr_times(d_inode(file->f_path.dentry),
				file_inode(lower_file));
	}
out:
	return err;
}

const struct file_operations trfs_main_fops = {
	.llseek		= generic_file_llseek,
	.read		= trfs_read,
	.write		= trfs_write,
	.unlocked_ioctl	= trfs_unlocked_ioctl,
#ifdef CONFIG_COMPAT
	.compat_ioctl	= trfs_compat_ioctl,
#endif
	.mmap		= trfs_mmap,
	.open		= trfs_open,
	.flush		= trfs_flush,
	.release	= trfs_file_release,
	.fsync		= trfs_fsync,
	.fasync		= trfs_fasync,
	.read_iter	= trfs_read_iter,
	.write_iter	= trfs_write_iter,
};

/* trimmed directory options */
const struct file_operations trfs_dir_fops = {
	.llseek		= trfs_file_llseek,
	.read		= generic_read_dir,
	.iterate	= trfs_readdir,
	.unlocked_ioctl	= trfs_unlocked_ioctl,
#ifdef CONFIG_COMPAT
	.compat_ioctl	= trfs_compat_ioctl,
#endif
	.open		= trfs_open,
	.release	= trfs_file_release,
	.flush		= trfs_flush,
	.fsync		= trfs_fsync,
	.fasync		= trfs_fasync,
};
