#include <linux/fs.h>
#include <linux/fs_stack.h>
#include <linux/namei.h>
#include <linux/hash.h>

#define MD5_DIGEST_SIZE 16
#define TRFS_DEFAULT_HASH "md5"

void trfs_to_hex(char *dst, char *src, size_t src_size);
int trfs_calculate_md5(char *dst, char *src, int len);

