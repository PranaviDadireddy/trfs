/*
 * Copyright (c) 1998-2015 Erez Zadok
 * Copyright (c) 2009	   Shrikar Archak
 * Copyright (c) 2003-2015 Stony Brook University
 * Copyright (c) 2003-2015 The Research Foundation of SUNY
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "trfs.h"
#include <linux/module.h>
#include <linux/parser.h>

DECLARE_BITMAP(fs_bitmap, MAX_BITS);
u32 record_id;
enum { trfs_opt_tfile,
	trfs_opt_err };

static const match_table_t trfs_tokens = {
	{trfs_opt_tfile, "tfile=%s"},
	{trfs_opt_err, NULL}
};

/**
 * trfs_parse_options
 * @options: The options passed to the kernel
 * @tfile_path: stores trace file path provided by user
 *
 * Parse mount options:
 * tfile=path         - trace file path to log the records
 *
 * Returns the trace file path to which the tracing records need
 * to be logged
 * Returns zero on success; -EINVAL on error
 */
static int trfs_parse_options(char *opt, char **tfile_path)
{
	char *p;
	substring_t args[MAX_OPT_ARGS];
	int token;

	while ((p = strsep(&opt, ",")) != NULL) {
		if (!*p)
			continue;
		token = match_token(p, trfs_tokens, args);
		switch (token) {
		case trfs_opt_tfile:
			*tfile_path = args[0].from;
			printk(KERN_DEBUG "option : %s\n", p);
			break;
		case trfs_opt_err:
		default:
			printk(KERN_WARNING
					"%s: trfs: unrecognized option [%s]\n",
					__func__, p);
			return -EINVAL;
		}
	}
	if (*tfile_path == NULL) {
		printk(KERN_ERR "trfs: missing option\n");
		return -EINVAL;
	}

	return 0;
}

/*
 * There is no need to lock the trfs_super_info's rwsem as there is no
 * way anyone can have a reference to the superblock at this point in time.
 */
static int trfs_read_super(struct super_block *sb, void *data, int silent)
{
	int err = 0;
	struct super_block *lower_sb;
	struct trfs_mount_data *mount_data = data;
	char *tfile_path = NULL;
	struct path lower_path;
	char *dev_name = (char *) mount_data->path;
	struct inode *inode;
	struct file *filp;

	/* parse mount options and get tfile path */
	err = trfs_parse_options((char *) mount_data->data, &tfile_path);
	if (err)
		goto out;

	if (!dev_name) {
		printk(KERN_ERR
				"trfs: read_super: missing dev_name argument\n");
		err = -EINVAL;
		goto out;
	}

	/* parse lower path */
	err = kern_path(dev_name, LOOKUP_FOLLOW | LOOKUP_DIRECTORY,
			&lower_path);
	if (err) {
		printk(KERN_ERR	"trfs: error accessing "
				"lower directory '%s'\n", dev_name);
		goto out;
	}

	/* open the trace file */
	filp = filp_open(tfile_path, O_RDWR | O_TRUNC, 0644);
	if (IS_ERR(filp)) {
		err = PTR_ERR(filp);
		printk("Error while opening tfile errno.:%d\n", err);
		goto out;
	}
	filp->f_pos = 0;

	/* create background thread */
	err = trfs_init_kthread();
	if (err) {
		printk(KERN_ERR "%s: kthread initialization failed; "
				"err = [%d]\n", __func__, err);
		goto out_free;
	}

	/* allocate superblock private data */
	sb->s_fs_info = kzalloc(sizeof(struct trfs_sb_info), GFP_KERNEL);
	if (!TRFS_SB(sb)) {
		printk(KERN_CRIT "trfs: read_super: out of memory\n");
		err = -ENOMEM;
		goto out_destroy_kthread;
	}

	/* set the lower superblock field of upper superblock */
	lower_sb = lower_path.dentry->d_sb;
	atomic_inc(&lower_sb->s_active);
	trfs_set_lower_super(sb, lower_sb, filp, fs_bitmap);

	/* inherit maxbytes from lower file system */
	sb->s_maxbytes = lower_sb->s_maxbytes;

	/*
	 * Our c/m/atime granularity is 1 ns because we may stack on file
	 * systems whose granularity is as good.
	 */
	sb->s_time_gran = 1;

	sb->s_op = &trfs_sops;

	sb->s_export_op = &trfs_export_ops; /* adding NFS support */

	/* get a new inode and allocate our root dentry */
	inode = trfs_iget(sb, d_inode(lower_path.dentry));
	if (IS_ERR(inode)) {
		err = PTR_ERR(inode);
		goto out_sput;
	}
	sb->s_root = d_make_root(inode);
	if (!sb->s_root) {
		err = -ENOMEM;
		goto out_iput;
	}
	d_set_d_op(sb->s_root, &trfs_dops);

	/* link the upper and lower dentries */
	sb->s_root->d_fsdata = NULL;
	err = new_dentry_private_data(sb->s_root);
	if (err)
		goto out_freeroot;

	/* if get here: cannot have error */

	/* set the lower dentries for s_root */
	trfs_set_lower_path(sb->s_root, &lower_path);

	/*
	 * No need to call interpose because we already have a positive
	 * dentry, which was instantiated by d_make_root.  Just need to
	 * d_rehash it.
	 */
	d_rehash(sb->s_root);
	if (!silent)
		printk(KERN_INFO
				"trfs: mounted on top of %s type %s\n",
				dev_name, lower_sb->s_type->name);
	goto out; /* all is well */

	/* no longer needed: free_dentry_private_data(sb->s_root); */
out_freeroot:
	dput(sb->s_root);
out_iput:
	iput(inode);
out_sput:
	/* drop refs we took earlier */
	atomic_dec(&lower_sb->s_active);
	kfree(TRFS_SB(sb));
	sb->s_fs_info = NULL;
out_destroy_kthread:
	if (err)
		trfs_destroy_kthread();
out_free:
	path_put(&lower_path);
	if (err)
		filp_close(filp, NULL);
out:
	return err;
}
struct dentry *trfs_mount(struct file_system_type *fs_type, int flags,
		const char *dev_name, void *raw_data)
{
	struct trfs_mount_data mount_data = {
		.data = raw_data,
		.path = (void *) dev_name
	};

	return mount_nodev(fs_type, flags, &mount_data,
			trfs_read_super);
}

static struct file_system_type trfs_fs_type = {
	.owner		= THIS_MODULE,
	.name		= TRFS_NAME,
	.mount		= trfs_mount,
	.kill_sb	= generic_shutdown_super,
	.fs_flags	= 0,
};
MODULE_ALIAS_FS(TRFS_NAME);

static int __init init_trfs_fs(void)
{
	int err;

	pr_info("Registering trfs " TRFS_VERSION "\n");

	err = trfs_init_inode_cache();
	if (err)
		goto out;
	err = trfs_init_dentry_cache();
	if (err)
		goto out;
	err = register_filesystem(&trfs_fs_type);
out:
	if (err) {
		trfs_destroy_inode_cache();
		trfs_destroy_dentry_cache();
	}
	return err;
}

static void __exit exit_trfs_fs(void)
{
	trfs_destroy_inode_cache();
	trfs_destroy_dentry_cache();
	unregister_filesystem(&trfs_fs_type);
	pr_info("Completed trfs module unload\n");
}

MODULE_AUTHOR("Erez Zadok, Filesystems and Storage Lab, Stony Brook University"
		" (http://www.fsl.cs.sunysb.edu/)");
MODULE_DESCRIPTION("Trfs " TRFS_VERSION
		" (http://trfs.filesystems.org/)");
MODULE_LICENSE("GPL");

module_init(init_trfs_fs);
module_exit(exit_trfs_fs);
