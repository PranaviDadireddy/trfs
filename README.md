PROBLEM 

Stackable, tracing file system called "trfs". Trfs should capture 
records of file system activity for a number of file system operations.

*******************************************************************************
DIRECTORIES AND FILES INCLUDED IN SUBMISSION

1. /usr/src/hw2-cse506g24/fs/trfs/
   We have kept our entire tracing file system code in this folder.
2. /usr/src/hw2-cse506g24/hw2/
   treplay.c - this will read in a tfile, and replay its operations one by one.
   trctl.c - Takes in operations to be enabled for tracing 
	     and sets the bitmap in Kernel
3. /usr/src/hw2-cse506g24/include/uapi/linux/trfs.h - includes the headers for IOCTL
********************************************************************************

USAGE AND TESTING

Compiling trfs:

1. Clone hw2-cse506g24, cd hw2-cse506g24, copy hw2/Kernel.config  to hw2-cse506g24/.config i
   and run the following commands -
		make
		make modules_install
		make install
 
2. Run 'make headers_install ARCH=i386 INSTALL_HDR_PATH=/usr' 
   to compile trfs.h in hw2-cse506g24/uapi/linux/
3. cd hw2-cse506g24/fs/trfs 
4. make
5. sh install_modules.sh

Compiling treplay.c and trctl.c

1. cd hw2-cse506g24/hw2
2. make 

Create tfile and directories for testing

6. Create a tfile1 in /tmp
7. Create a directory with a bunch of existing files, folder, etc. and some 
   content, say DIR1
8. make a copy of DIR1 to DIR2
9. cp -a DIR1 DIR2

Mount trfs

10. mount -t trfs -o tfile=/tmp/tfile.txt /usr/src/hw2-cse506g24/dir1/ /mnt/trfs/
11. cd /mnt/trfs
12. run a bunch of unix commands to read, write, create, delete, 
    rename, etc. files.
13. Run umount /usr/src/hw2-cse506g24/dir1/

Run your replayer in DIR2

14. cd DIR2
15. ./treplay_path /mnt/trfs

Incremental IOCTL

1. Uncomment the #define EXTRA_CREDIT in hw2/trctl.c
3. make 
2. Valid arguments are +/-event_name, all, none
	Eg: ./trctl +read -write all /mnt/trfs
  

*******************************************************************************

F/S EVENTS INTERCEPTED

1. mkdir
2. rmdir
3. open
4. close
5. read
6. write
7. rename
8. link
9. unlink
10. symlink
11. flush
12. release

*******************************************************************************

ASSUMPTIONS

treplay

1. n ,s are not given at once 
   Reason: There is no chance of aborting when we are not even replaying but 
   just displaying the records from tfile when n option is given.

trctl

1. For incremental ioctl (***EXTRA_CREDIT***)
   Valid arguments are +/-event_name, all, none
   Eg: ./trctl +read -write all /mnt/trfs

trfs

1. /tmp/tfile.txt is already created
2. If a file operation is successful but tracing fails, success is returned 
   to user but traced record is not written to tfile.
   Hence the the operation is not replayed.
   Eg: mkdir operation is successful but ENOMEM is returned 
   during malloc in tracing, mkdir is not replayed.
3. During read/write operations, maximum length of file is 4KB.

*******************************************************************************

DESIGN

trfs

1. When trfs is mounted, kthread(trfs_kthread) is started and tfile is opened.
   Pointer to tfile is stored in superblock
1. A structure corresponding to every operation(read, write etc.) is maintained
2. When an operation is intercepted, structure is filled with data gathered 
   and a write_to_file function is called.
3. write_to_file function creates requests for every record and adds it to 
   the wait queue.
4. thread_fn keeps checking the waitqueue and picks a request added 
   in wait queue. Then record is written to tfile through vfs_write 
4. To maintain consistency, mutex lock is taken while adding the record to 
   wait queue.
5. When trfs is unmounted, kthread is destroyed and tfile is closed.

******************************************************************************
VALIDATIONS

1. In the record of each operation, return value is stored. Replay is
   successful when the return value stored in record matched the return 
   value of replay call. 
2. For write operations file data is stored in tfile. For read operation MD5 
   checksum is calculated and stored in record. Checksum is also calculated 
   for data read from replayed read call. This checksum is validated with checksum
   stored in record.
3. The record id is maintained as an extern variable and a spin lock is used to access
   and increment the record id so that each record written to tfile has a unique id.
4. When handling the read and write cases, there may be a case where there are a series
   of opens and then a read/write, we need to know the corresponding fd with which the 
   read/write call should be replayed. For this purpose, we maintaned a linked list
   to store the (process id and file ptr) and file descriptor returned by a open syscall.
   When we get a write/read call, we lookup the linked list for the corresponding fd with 
   the process id and file ptr value from read/write record and then replay the read/
   write syscall.
5. When the release function is intercepted, we are replaying close syscall and 
   deleting the corresponding fd from the linked list.

**************************************************************************************
MAPPING - We have mapped the functions to the following hex values.

MKDIR ---------------------	0x001

RMDIR ---------------------     0x002

OPEN  ---------------------     0x004

CLOSE ---------------------     0x008

READ  ---------------------     0x010

WRITE ---------------------     0x020

RENAME---------------------     0x040

LINK  ---------------------     0x080

UNLINK---------------------     0x100

SYMLINK---------------------    0x200

FLUSH ---------------------     0x400

RELEASE---------------------    0x800


******************************************************************************************


