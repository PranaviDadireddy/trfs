#include<stdio.h>
#include<unistd.h>
#include<ctype.h>
#include<stdlib.h>
#include<fcntl.h>
#include<string.h>
#include<stdint.h>
#include<sys/stat.h>
#include<sys/types.h>
#include <errno.h>
#include<linux/trfs.h>
#include<stdbool.h>
#include <openssl/md5.h>
#include "treplay.h"

/* struct node for tracking file descriptors */
struct node {
	unsigned long pid;
	unsigned long file_ptr;
	int fd;
	struct node *next;
};

struct node *head;
struct node *current;

/* insert node in the front of linked list */
void insertFront(unsigned long pid, unsigned long file_ptr, int fd)
{
	struct node *temp = (struct node *) malloc(sizeof(struct node));

	temp->pid = pid;
	temp->file_ptr = file_ptr;
	temp->fd = fd;

	temp->next = head;
	head = temp;
}

/* print linked list */
void printList(void)
{
	struct node *ptr = head;

	while (ptr != NULL) {
		printf("(%ld, %ju, %d) ", ptr->pid, ptr->file_ptr, ptr->fd);
		ptr = ptr->next;
	}
	printf("\n");
}

/* returns file fd for corresponding process id and file ptr */
int find_fd(unsigned long pid, unsigned long file_ptr)
{
	struct node *current = head;

	if (head == NULL)
		return -1;
	while ((current->pid != pid) || (current->file_ptr != file_ptr)) {
		if (current->next == NULL)
			return -1;
		else
			current = current->next;
	}
	return current->fd;
}

/* deletes the node with given process id and file ptr */
struct node *delete(unsigned long pid, unsigned long file_ptr)
{
	struct node *current = head;
	struct node *previous = NULL;

	if (head == NULL)
		return NULL;

	while ((current->pid != pid)  || (current->file_ptr != file_ptr)) {
		if (current->next == NULL)
			return NULL;
		else {
			previous = current;
			current = current->next;
		}
	}

	if (current == head)
		head = head->next;
	else
		previous->next = current->next;
	return current;
}

char *string_to_md5_hex(const char *str, int length)
{
	    int n;
	    MD5_CTX c;
	    unsigned char digest[16];
	    char *out = (char *)malloc(33);

	    MD5_Init(&c);

	    while (length > 0) {
		if (length > 512) {
		    MD5_Update(&c, str, 512);
		} else {
		    MD5_Update(&c, str, length);
		}
		length -= 512;
		str += 512;
	    }

	    MD5_Final(digest, &c);

	    for (n = 0; n < 16; ++n)
		snprintf(&(out[n*2]), 16*2, "%02x", (unsigned int)digest[n]);

	    return out;
}


int main(int argc, char **argv)
{
	int c;
	int n = 0;
	int s = 0;
	int err = 0;
	char *logFilename = NULL;

	if (argc < 2) {
		printf("Invalid number of arguments (tfile is expected)\n");
		exit(0);
	}

	/* get command line options
	 * valid options - n, s
	 */
	while ((c = getopt(argc, argv, "ns")) != -1) {
		switch (c) {
		case 'n':
			n = 1;
			break;
		case 's':
			s = 1;
			break;
		case '?':
			if (optopt == 'c')
				fprintf(stderr, "Option -%c "
						"requires an argument.\n", optopt);
			else if (isprint (optopt))
				fprintf(stderr, "Unknown option "
						"`-%c'.\n", optopt);
			else
				fprintf(stderr, "Unknown option character "
						"`\\x%x'.\n", optopt);
			return 1;
		default:
			abort();

		}
	}
	if (n == 1 && s == 1) {
		printf("Invalid combination of options -n and -s are set\n");
		return 0;
	}
	logFilename = argv[optind];
	err = traceplay(logFilename, n, s);
	if (errno < 0) {
		perror("Treplay Failed ");
		printf("Treplay returned %d (errno=%d)\n", err, errno);
	}
	return err;
}

/* traceplay - reads records from tfile and replays them
 * @filename - tfile to be read
 * @n - show the replayed records
 * @s - strict mode, abort if failur
 *
 * calls the replay function
 *
 * Returns 0 on success and non-negative on failure
 */
int traceplay(char *filename, int n, int s)
{
	int fd;
	int err = 0;
	void *rec = NULL;
	char *pathname = NULL, *old_pathname = NULL, *new_pathname = NULL;
	char *buf_write = NULL;
	char *buf_read = NULL;
	uint8_t type;
	uint16_t size;
	long offset = 0;

	fd = open(filename, O_RDONLY, 0644);
	if (fd < 0) {
		printf("File open error\n");
		err = fd;
		goto out;
	}

	err = read(fd, &size, sizeof(uint16_t));
	if (err < 0)
		goto out;
	err = read(fd, &type, sizeof(uint8_t));
	if (err < 0)
		goto out;

	while (err != 0) {
		switch (type) {
		case TYPE_MKDIR:
			rec = malloc(size);
			err = pread(fd, rec, size, offset);
			offset += size;
			if (err < 0)
				goto out;

			pathname = malloc(((struct mkdir_record *)rec)->len_pathname);
			
			strcpy(pathname,
					((struct mkdir_record *)rec)->pathname + 1);
			if (err < 0)
				goto out;

			err = replay_syscall((void *)rec, pathname, type, n, s);
			break;

		case TYPE_RMDIR:
			rec = malloc(size);
			err = pread(fd, rec, size, offset);
			offset += size;
			if (err < 0)
				goto out;

			pathname = malloc(((struct rmdir_record *)rec)->len_pathname);
			
			strcpy(pathname,
					((struct rmdir_record *)rec)->pathname + 1);
			if (err < 0)
				goto out;

			err = replay_syscall((void *)rec, pathname,
					type, n, s);
			break;

		case TYPE_OPEN:
			rec = malloc(size);
			err = pread(fd, rec, size, offset);
			offset += size;
			if (err < 0)
				goto out;

			pathname = malloc(((struct record_open *)rec)->len_pathname);
			if (!strcmp(((struct record_open *)rec)->pathname, "/"))
				strcpy(pathname, ".");
			else
				strcpy(pathname,
					((struct record_open *)rec)->pathname + 1);
			if (err < 0)
				goto out;

			err = replay_syscall((void *)rec, pathname,
					type, n, s);
			break;

		case TYPE_WRITE:
			rec = malloc(size);
			err = pread(fd, rec, size, offset);
			offset += size;
			if (err < 0)
				goto out;

			pathname = malloc(((struct record_write *)rec)->len_pathname);
			strcpy(pathname, ((struct record_write *)rec)->pathname + 1);
			buf_write = malloc(((struct record_write *)rec)->buf_len);
			err = pread(fd, buf_write,
					((struct record_write *)rec)->buf_len, offset);
			offset = offset + ((struct record_write *)rec)->buf_len;

			if (err < 0)
				goto out;

			err = replay_syscall_read_write((void *)rec, pathname,
					buf_write, type, n, s);

			break;

		case TYPE_READ:
			rec = malloc(size);
			err = pread(fd, rec, size, offset);
			offset += size;
			if (err < 0)
				goto out;

			pathname = malloc(((struct record_read *)rec)->len_pathname);
			strcpy(pathname, ((struct record_read *)rec)->pathname + 1);

			buf_read = malloc(((struct record_read *)rec)->buf_len);
			err = pread(fd, buf_read,
					((struct record_read *)rec)->buf_len, offset);
			offset = offset + ((struct record_read *)rec)->buf_len;
			if (err < 0)
				goto out;
			err = replay_syscall_read_write((void *)rec, pathname,
					buf_read, type, n, s);
			break;

		case TYPE_LINK:
			rec = malloc(size);
			err = pread(fd, rec, size, offset);
			offset += size;
			if (err < 0)
				goto out;

			pathname = malloc(((struct record_link *)rec)->len_old_pathname
					+ ((struct record_link *)rec)->len_new_pathname);
			old_pathname = malloc(((struct record_link *)rec)->len_old_pathname);
			new_pathname = malloc(((struct record_link *)rec)->len_new_pathname);

			strncpy(old_pathname, ((struct record_link *)rec)->pathname + 1,
					((struct record_link *)rec)->len_old_pathname - 1);
			strncpy(new_pathname, ((struct record_link *)rec)->pathname
					+ ((struct record_link *)rec)->len_old_pathname + 1,
					((struct record_link *)rec)->len_new_pathname - 1);
			if (err < 0)
				goto out;
			err = replay_syscall_multiple((void *)rec, old_pathname,
					new_pathname, type, n, s);
			break;

		case TYPE_UNLINK:
			rec = malloc(size);
			err = pread(fd, rec, size, offset);
			offset += size;
			if (err < 0)
				goto out;

			pathname = malloc(((struct record_unlink *)rec)->len_pathname);
			strcpy(pathname, ((struct record_unlink *)rec)->pathname + 1);
			if (err < 0)
				goto out;

			err = replay_syscall((void *)rec, pathname, type,
					n, s);
			break;

		case TYPE_SYMLINK:
			rec = malloc(size);
			err = pread(fd, rec, size, offset);
			if (err < 0)
				goto out;

			offset += size;

			pathname = malloc(((struct record_symlink *)rec)->len_old_pathname
					+ ((struct record_symlink *)rec)->len_new_pathname);
			old_pathname = malloc(((struct record_symlink *)rec)->len_old_pathname);
			new_pathname = malloc(((struct record_symlink *)rec)->len_new_pathname);

			strncpy(old_pathname,
					((struct record_symlink *)rec)->pathname,
					((struct record_symlink *)rec)->len_old_pathname);
			strncpy(new_pathname,
					((struct record_symlink *)rec)->pathname
					+ ((struct record_symlink *)rec)->len_old_pathname + 1,
					((struct record_symlink *)rec)->len_new_pathname);
			if (err < 0)
				goto out;
			err = replay_syscall_multiple((void *)rec,
					old_pathname, new_pathname, type, n, s);
			break;

		case TYPE_RENAME:
			rec = malloc(size);
			err = pread(fd, rec, size, offset);
			offset += size;
			if (err < 0)
				goto out;

			pathname = malloc(((struct record_rename *)rec)->len_old_pathname
					+ ((struct record_rename *)rec)->len_new_pathname);
			old_pathname = malloc(((struct record_rename *)rec)->len_old_pathname);
			new_pathname = malloc(((struct record_rename *)rec)->len_new_pathname);

			strncpy(old_pathname, ((struct record_link *)rec)->pathname + 1,
					((struct record_link *)rec)->len_old_pathname - 1);
			strncpy(new_pathname, ((struct record_link *)rec)->pathname
					+ ((struct record_link *)rec)->len_old_pathname + 1,
					((struct record_link *)rec)->len_new_pathname - 1);
			if (err < 0)
				goto out;
			err = replay_syscall_multiple((void *)rec, old_pathname,
					new_pathname, type, n, s);
			break;

		case TYPE_FLUSH:

			rec = malloc(size);
			err = pread(fd, rec, size, offset);
			offset += size;
			if (err < 0) {
				goto out;
			}

			pathname = malloc(((struct record_flush *)rec)->len_pathname);
			if (!strcmp(((struct record_flush *)rec)->pathname, "/"))
				strcpy(pathname, ".");
			else
				strcpy(pathname, ((struct record_flush *)rec)->pathname + 1);
			if (err < 0) {
				goto out;
			}

			err = replay_syscall((void *)rec, pathname, type, n, s);
			break;

		case TYPE_RELEASE:

			rec = malloc(size);
			err = pread(fd, rec, size, offset);
			offset += size;
			if (err < 0) {
				goto out;
			}

			pathname = malloc(((struct record_release *)rec)->len_pathname);
			if (!strcmp(((struct record_release *)rec)->pathname, "/"))
				strcpy(pathname, ".");
			else
				strcpy(pathname, ((struct record_release *)rec)->pathname + 1);
			if (err < 0) {
				goto out;
			}

			err = replay_syscall((void *)rec, pathname, type, n, s);
			break;

		default:
			exit(0);

		}

		lseek(fd, offset, SEEK_SET);
		err = read(fd, &size, sizeof(uint16_t));
		if (err < 0)
			goto out;

		err = read(fd, &type, sizeof(uint8_t));
		if (err < 0)
			goto out;
	}

out:
	if (rec)
		free(rec);
	if (pathname)
		free(pathname);
	if (fd > 0)
		close(fd);
	return err;

}

/* replays records of type link, symlink and rename */
int replay_syscall_multiple(void *rec, char *old_pathname, char *new_pathname,
		uint8_t type, int n, int s)

{
	int ret, err;

	switch (type) {
	case TYPE_LINK:
		if (n == 0 && s == 0) {

			display_record_multiple(rec, old_pathname,
					new_pathname, type);
			printf("Replaying link(%s, %s)\n",
					old_pathname, new_pathname);
			err = link(old_pathname, new_pathname);
			printf("Output after replaying : %d\n", err);

			if (err == ((struct record_link *)rec)->ret_val) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("Failure\n");
				ret = 0;
				goto out;
			}

		} else if (n == 1) {
			display_record_multiple(rec, old_pathname,
					new_pathname, type);
			goto out;
		} else if (s == 1) {
			display_record_multiple(rec, old_pathname,
					new_pathname, type);
			printf("Replaying link(%s, %s)\n",
					old_pathname, new_pathname);
			err = link(old_pathname, new_pathname);
			printf("Output after replaying : %d\n", err);
			if (err == ((struct record_link *)rec)->ret_val) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("err:%d\n", err);
				printf("Failure\n");
				ret = -1;
				exit(0);
			}
		}
		break;

	case TYPE_SYMLINK:
		if (n == 0 && s == 0) {
			display_record_multiple(rec, old_pathname,
					new_pathname, type);
			printf("Replaying symlink(%s, %s)\n",
					old_pathname, new_pathname);
			err = symlink(old_pathname, new_pathname);
			printf("Output after replaying : %d\n", err);

			if (err == ((struct record_symlink *)rec)->ret_val) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("Failure\n");
				ret = 0;
				goto out;
			}

		} else if (n == 1) {
			display_record_multiple(rec, old_pathname,
					new_pathname, type);
			goto out;
		} else if (s == 1) {
			display_record_multiple(rec, old_pathname,
					new_pathname, type);
			printf("Replaying symlink(%s, %s)\n",
					old_pathname, new_pathname);
			err = symlink(old_pathname, new_pathname);
			printf("Output after replaying : %d\n", err);
			if (err == ((struct record_symlink *)rec)->ret_val) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("err:%d\n", err);
				printf("Failure\n");
				ret = -1;
				exit(0);
			}
		}
		break;

	case TYPE_RENAME:
		if (n == 0 && s == 0) {
			display_record_multiple(rec, old_pathname,
					new_pathname, type);
			printf("Replaying rename(%s, %s)\n",
					old_pathname, new_pathname);
			err = rename(old_pathname, new_pathname);
			printf("Output after replaying : %d\n", err);

			if (err == ((struct record_rename *)rec)->ret_val) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("Failure\n");
				ret = 0;
				goto out;
			}

		} else if (n == 1) {
			display_record_multiple(rec, old_pathname,
					new_pathname, type);
			goto out;
		} else if (s == 1) {
			display_record_multiple(rec, old_pathname,
					new_pathname, type);
			printf("Replaying rename(%s, %s)\n",
					old_pathname, new_pathname);
			err = rename(old_pathname, new_pathname);
			printf("Output after replaying : %d\n", err);
			if (err == ((struct record_rename *)rec)->ret_val) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("Failure\n");
				ret = -1;
				exit(0);
			}
		}
		break;
	}
out:
	return ret;

}

/* replays records of type read and write */
int replay_syscall_read_write(void *rec, char *pathname, char *buffer,
		uint8_t type, int n, int s)
{
	int ret, err;
	int fd_write;
	int fd_read;
	char *read_buffer = NULL;
	char *md5_hex = NULL;

	switch (type) {
	case TYPE_WRITE:
		if (n == 0 && s == 0) {
			display_record(rec, pathname, type);
			fd_write = find_fd(((struct record_write *)rec)->pid,
					((struct record_write *)rec)->file_ptr);
			if (fd_write != -1) {
				printf("Replaying write(%d, data, %d)\n",
						fd_write, ((struct record_write *)rec)->buf_len);
				err = write(fd_write, buffer,
						((struct record_write *)rec)->buf_len);
				printf("Output after replaying : %d\n", err);

				if (err == ((struct record_write *)rec)->ret_val) {
					printf("Success\n");
					ret = 0;
					goto out;
				} else {
					printf("Failure\n");
					ret = 0;
					goto out;
				}

			} else {
				printf("write(): Untraceable record because "
						"corresponding file descriptor is not found \n");
			}

		} else if (n == 1) {
			display_record(rec, pathname, type);
			goto out;
		} else if (s == 1) {
			display_record(rec, pathname, type);
			fd_write = find_fd(((struct record_write *)rec)->pid,
					((struct record_write *)rec)->file_ptr);
			if (fd_write != -1) {
				printf("Replaying write(%d, data, %d)\n",
						fd_write, ((struct record_write *)rec)->buf_len);
				err = write(fd_write, buffer,
						((struct record_write *)rec)->buf_len);
				printf("Output after replaying : %d\n", err);

				if (err == ((struct record_write *)rec)->ret_val) {
					printf("Success\n");
					ret = 0;
					goto out;
				} else {
					printf("Failure\n");
					ret = -1;
					exit(0);
				}

			} else {
				printf("write(): Untraceable record because "
						"corresponding file descriptor is not found \n");
			}
		}

		break;

	case TYPE_READ:
		if (n == 0 && s == 0) {
			display_record(rec, pathname, type);

			fd_read = find_fd(((struct record_read *)rec)->pid,
					((struct record_read *)rec)->file_ptr);
			if (fd_read != -1) {
				printf("Replaying read(%d,buf, %d)\n",
						fd_read, ((struct record_read *)rec)->buf_len);

				read_buffer = malloc(((struct record_read *)rec)->buf_len);
				err = read(fd_read, read_buffer,
						((struct record_read *)rec)->buf_len);

				md5_hex = string_to_md5_hex(read_buffer, strlen(read_buffer));
				printf("Output after replaying : %d\n", err);

				if ((err == ((struct record_read *)rec)->ret_val)
						&& !(strcmp(md5_hex, buffer))) {
					printf("Success\n");
					ret = 0;
					goto out;
				} else {
					printf("Failure\n");
					ret = 0;
					goto out;
				}

			} else {
				printf("read(): Untraceable record because "
						"corresponding file descriptor is not found \n");
			}

		} else if (n == 1) {
			display_record(rec, pathname, type);
			goto out;
		} else if (s == 1) {
			display_record(rec, pathname, type);

			fd_read = find_fd(((struct record_read *)rec)->pid,
					((struct record_read *)rec)->file_ptr);
			if (fd_read != -1) {
				printf("Replaying read(%d,buf, %d)\n",
						fd_read, ((struct record_read *)rec)->buf_len);

				read_buffer = malloc(((struct record_read *)rec)->buf_len);
				err = read(fd_read, read_buffer,
						((struct record_read *)rec)->buf_len);

				md5_hex = string_to_md5_hex(read_buffer, strlen(read_buffer));
				printf("Output after replaying : %d\n", err);

				if ((err == ((struct record_read *)rec)->ret_val)
						&& !(strcmp(md5_hex, buffer))) {
					printf("Success\n");
					ret = 0;
					goto out;
				} else {
					printf("Failure\n");
					ret = -1;
					exit(0);
				}

			} else {
				printf("read(): Untraceable record because "
						"corresponding file descriptor is not found \n");
			}

		}
		if (md5_hex)
			free(md5_hex);
		break;
	}

out:
	return ret;
}

/* replays other records */
int replay_syscall(void *rec, char *pathname, uint8_t type, int n, int s)
{
	int ret, err;
	int fd_flush;
	int fd_release;

	switch (type) {
	case TYPE_MKDIR:

		if (n == 0 && s == 0) {
			display_record(rec, pathname, type);
			printf("Replaying mkdir(%s, %u)\n",
				pathname, ((struct mkdir_record *)rec)->mode);
			err = mkdir(pathname, ((struct mkdir_record *)rec)->mode);
			printf("Output after replaying : %d\n", err);

			if (err == ((struct mkdir_record *)rec)->ret_val) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("Failure\n");
				ret = 0;
				goto out;
			}

		} else if (n == 1) {
			display_record(rec, pathname, type);
			goto out;
		} else if (s == 1) {
			display_record(rec, pathname, type);
			printf("Replaying mkdir(%s, %u)\n",
				pathname, ((struct mkdir_record *)rec)->mode);
			err = mkdir(pathname, ((struct mkdir_record *)rec)->mode);
			printf("Output after replaying : %d\n", err);
			if (err == ((struct mkdir_record *)rec)->ret_val) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("Failure\n");
				ret = -1;
				exit(0);
			}
		}
		break;

	case TYPE_RMDIR:
		if (n == 0 && s == 0) {
			display_record(rec, pathname, type);
			printf("Replaying rmdir(%s)\n", pathname);
			err = rmdir(pathname);
			printf("Output after replaying : %d\n", err);

			if (err == ((struct rmdir_record *)rec)->ret_val) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("Failure\n");
				ret = 0;
				goto out;
			}
		} else if (n == 1) {
			display_record(rec, pathname, type);
			goto out;
		} else if (s == 1) {
			display_record(rec, pathname, type);
			printf("Replaying rmdir(%s)\n", pathname);
			err = rmdir(pathname);
			printf("Output after replaying : %d\n", err);
			if (err == ((struct rmdir_record *)rec)->ret_val) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("Failure\n");
				ret = -1;
				exit(0);
			}
		}
		break;

	case TYPE_OPEN:
		if (n == 0 && s == 0) {
			display_record(rec, pathname, type);
			printf("Replaying open(%s, %u, %u)\n",
					pathname, ((struct record_open *)rec)->flags,
					((struct record_open *)rec)->mode);
			err = open(pathname, ((struct record_open *)rec)->flags,
					((struct record_open *)rec)->mode);
			if (err != -1)
				insertFront(((struct record_open *)rec)->pid, ((struct record_open *)rec)->file_ptr, err);
			printf("Output after replaying : %d\n", err);

			if (err > 0) {
				printf("Success\n");
				ret = 0;
				goto out;
			}
		} else if (n == 1) {
			display_record(rec, pathname, type);
			goto out;
		} else if (s == 1) {
			display_record(rec, pathname, type);
			printf("Replaying open(%s, %u, %u)\n",
					pathname, ((struct record_open *)rec)->flags,
					((struct record_open *)rec)->mode);
			err = open(pathname, ((struct record_open *)rec)->flags,
					((struct record_open *)rec)->mode);
			if (err != -1)
				insertFront(((struct record_open *)rec)->pid, ((struct record_open *)rec)->file_ptr, err);
			printf("Output after replaying : %d\n", err);
			if (err > 0) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("Failure\n");
				ret = -1;
				exit(0);
			}
		}
		break;

	case TYPE_UNLINK:
		if (n == 0 && s == 0) {
			display_record(rec, pathname, type);
			printf("Replaying unlink(%s)\n", pathname);
			err = unlink(pathname);
			printf("Output after replaying : %d\n", err);

			if (err == ((struct record_unlink *)rec)->ret_val) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("Failure\n");
				ret = 0;
				goto out;
			}

		} else if (n == 1) {
			display_record(rec, pathname, type);
			goto out;
		} else if (s == 1) {
			display_record(rec, pathname, type);
			printf("Replaying unlink(%s)\n", pathname);
			err = unlink(pathname);
			printf("Output after replaying : %d\n", err);
			if (err == ((struct record_unlink *)rec)->ret_val) {
				printf("Success\n");
				ret = 0;
				goto out;
			} else {
				printf("Failure\n");
				ret = -1;
				exit(0);
			}
		}
		break;

	case TYPE_FLUSH:
		if (n == 0 && s == 0) {
			display_record(rec, pathname, type);
			fd_flush = find_fd(((struct record_flush *)rec)->pid,
					((struct record_flush *)rec)->file_ptr);
			if (fd_flush != -1) {
				printf("Replaying fsync(%d)\n", fd_flush);
				err = fsync(fd_flush);
				if (err != -1)
				printf("Output after replaying : %d\n", err);

				if (err == ((struct record_flush *)rec)->ret_val) {
					printf("Success\n");
					ret = 0;
					goto out;
				}

			} else {
				printf("fsync(): Untraceable record because "
						"corresponding file descriptor is not found \n");
			}
		} else if (n == 1) {
			display_record(rec, pathname, type);
			goto out;
		} else if (s == 1) {
			display_record(rec, pathname, type);
			fd_flush = find_fd(((struct record_flush *)rec)->pid,
					((struct record_flush *)rec)->file_ptr);
			if (fd_flush != -1) {
				printf("Replaying fsync(%d)\n", fd_flush);
				err = fsync(fd_flush);
				if (err != -1)
				printf("Output after replaying : %d\n", err);

				if (err == ((struct record_flush *)rec)->ret_val) {
					printf("Success\n");
					ret = -1;
					exit(0);
				}

			} else {
				printf("fsync(): Untraceable record because "
						"corresponding file descriptor is not found \n");
			}

		}
		break;

	case TYPE_RELEASE:
		if (n == 0 && s == 0) {
			display_record(rec, pathname, type);
			fd_release = find_fd(((struct record_release *)rec)->pid,
					((struct record_release *)rec)->file_ptr);
			if (fd_release != -1) {
				printf("Replaying close(%d) \n", fd_release);
				err = close(fd_release);
				if (err != -1)
					delete(((struct record_release *)rec)->pid,
							((struct record_release *)rec)->file_ptr);
				printf("Output after replaying : %d \n", err);

				if (err == ((struct record_release *)rec)->ret_val) {
					printf("Success \n");
					ret = 0;
					goto out;
				}
			} else {
				printf("close(): Untraceable record because "
						"corresponding file descriptor is not found \n");
			}
		} else if (n == 1) {
			display_record(rec, pathname, type);
			goto out;
		} else if (s == 1) {
			display_record(rec, pathname, type);
			fd_release = find_fd(((struct record_release *)rec)->pid,
					((struct record_release *)rec)->file_ptr);
			if (fd_release != -1) {
				printf("Replaying close(%d) \n", fd_release);
				err = close(fd_release);
				if (err != -1)
					delete(((struct record_release *)rec)->pid,
							((struct record_release *)rec)->file_ptr);
				printf("Output after replaying : %d \n", err);

				if (err == ((struct record_release *)rec)->ret_val) {
					printf("Success \n");
					ret = -1;
					exit(0);
				}
			} else {
				printf("close(): Untraceable record because "
						"corresponding file descriptor is not found \n");
			}

		}
		break;
	}
out:
	return ret;
}

/* display records to user */
void display_record_multiple(void *rec, char *old_pathname, char *new_pathname, uint8_t type)
{
	switch (type) {
	case TYPE_LINK:
		printf("***Record { id : %d, type : link , size : %d, retval : %d,"
				" old_pathname : %s new_pathname : %s}\n",
				((struct record_link *)rec)->id,
				((struct record_link *)rec)->size,
				((struct record_link *)rec)->ret_val,
				old_pathname, new_pathname);
		break;

	case TYPE_RENAME:
		printf("***Record { id : %d, type : rename , size : %d, retval : %d,"
				" old_pathname : %s new_pathname : %s}\n",
				((struct record_link *)rec)->id,
				((struct record_link *)rec)->size,
				((struct record_link *)rec)->ret_val,
				old_pathname, new_pathname);
		break;
	}
}

void display_record(void *rec, char *pathname, uint8_t type)
{
	switch (type) {
	case TYPE_MKDIR:
		printf("***Record { id : %d, type : mkdir, size : %d, retval : %d,"
				"mode : %d, pathname : %s }\n",
				((struct mkdir_record *)rec)->id,
				((struct mkdir_record *)rec)->size,
				((struct mkdir_record *)rec)->ret_val,
				((struct mkdir_record *)rec)->mode, pathname);
		break;
	case TYPE_RMDIR:
		printf("***Record { id : %d, type : rmdir, size : %d, "
				"retval : %d, pathname : %s }\n",
				((struct rmdir_record *)rec)->id,
				((struct rmdir_record *)rec)->size,
				((struct rmdir_record *)rec)->ret_val, pathname);
		break;
	case TYPE_OPEN:
		printf("***Record { id : %u, type: open, mode : %u, flags : %u,"
				" pathname : %s, ret_val : %d }\n",
				((struct record_open *)rec)->id,
				((struct record_open *)rec)->mode,
				((struct record_open *)rec)->flags, pathname,
				((struct record_open *)rec)->ret_val);
		break;
	case TYPE_WRITE:
		printf("***Record { id : %u, type: write, mode : %u, flags : %u,"
				" pathname : %s, ret_val : %d }\n",
				((struct record_write *)rec)->id,
				((struct record_write *)rec)->mode,
				((struct record_write *)rec)->flags, pathname,
				((struct record_write *)rec)->ret_val);
		break;
	case TYPE_READ:
		printf("***Record { id : %u, type: read, mode : %u, flags : %u,"
				" pathname : %s, ret_val : %d }\n",
				((struct record_read *)rec)->id,
				((struct record_read *)rec)->mode,
				((struct record_read *)rec)->flags, pathname,
				((struct record_read *)rec)->ret_val);
		break;
	case TYPE_UNLINK:
		printf("***Record { id : %d, type : unlink, size : %d, "
				"retval : %d, pathname : %s }\n",
				((struct record_unlink *)rec)->id,
				((struct record_unlink *)rec)->size,
				((struct record_unlink *)rec)->ret_val, pathname);
		break;
	case TYPE_FLUSH:
		printf("***Record { id : %u, type: flush, pathname : %s, "
				"ret_val : %d }\n",
				((struct record_flush *)rec)->id, pathname,
				((struct record_flush *)rec)->ret_val);
		break;
	case TYPE_RELEASE:
                printf("***Record { id : %u, type: release, pathname : %s, "
                                "ret_val : %d }\n",
                                ((struct record_release *)rec)->id, pathname,
                                ((struct record_release *)rec)->ret_val);
                break;
	}
}

