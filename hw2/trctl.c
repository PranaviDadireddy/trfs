#include <stdio.h>
#include <linux/types.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/ioctl.h>
#include <linux/trfs.h>

/*#define EXTRA_CREDIT*/

/* set bit in bitmap*/
void set_bit(unsigned long *argp, unsigned long shift)
{
	*argp |= 1UL << shift;
}

/* unset bit in bitmap */
void unset_bit(unsigned long *argp, unsigned long shift)
{
	*argp &= ~(1UL << shift);
}

/* set bitmap based on the tracing operations
 * enabled/disabled
 */
void set_bitmap(unsigned long *argp, char *str)
{
	char c = str[0];
	unsigned int shift = 0;
	int i;

	if (!strcmp(str + 1, "mkdir"))
		shift = SHIFT_MKDIR;
	if (!strcmp(str + 1, "rmdir"))
		shift = SHIFT_RMDIR;
	if (!strcmp(str + 1, "open"))
		shift = SHIFT_OPEN;
	if (!strcmp(str + 1, "close"))
		shift = SHIFT_CLOSE;
	if (!strcmp(str + 1, "read"))
		shift = SHIFT_READ;
	if (!strcmp(str + 1, "write"))
		shift = SHIFT_WRITE;
	if (!strcmp(str + 1, "rename"))
		shift = SHIFT_RENAME;
	if (!strcmp(str + 1, "link"))
		shift = SHIFT_LINK;
	if (!strcmp(str + 1, "UNLINK"))
		shift = SHIFT_UNLINK;
	if (!strcmp(str + 1, "SYMLINK"))
		shift = SHIFT_SYMLINK;
	if (!strcmp(str, "all")) {
		for (i = 0; i < 64; i++)
			set_bit(argp, i);
	}
	if (!strcmp(str, "none")) {
		for (i = 0; i < 64; i++)
			unset_bit(argp, i);
	}
	if (shift != 0) {
		if (c == '+')
			set_bit(argp, shift);

		else
			unset_bit(argp, shift);
	}
}

int main(int argc, char *argv[])
{
	int fd;
	int err = 0;
	unsigned long hex_from_kernel = 0;
	unsigned long hex_from_user = 0;
	unsigned long *kargp = &hex_from_kernel;
	unsigned long *uargp = &hex_from_user;
	int i;
#ifdef EXTRA_CREDIT
	char *str = NULL;
#endif
	if (argc >= 2) {
		fd = open(argv[argc - 1], O_DIRECTORY, 0644);
		if (fd == -1) {
			perror("File open failed");
			err = fd;
			goto out;
		}
	}

	/* if <2 args are provided, incorrect no. of args
	 * if 2 args are provide - ioctl call to get bitmap
	 * if >2 args are provided - ioctl call to set bitmap based on the
	 * hex value
	 * EXTRA_CREDIT - ioctl calls to get bitmap and set it based on args
	 */
	if (argc == 2) {
		/* ioctl call */
		err = ioctl(fd, GET_BITMAP, kargp);
		if (err == -1) {
			printf("ioctl call failed errno : %d\n", errno);
			err = fd;
			goto out;
		}
		printf("0x%lx\n", *kargp);
	} else if (argc == 3) {
		if (!strcmp(argv[1], "all")) {
			for (i = 0; i < 64; i++)
				set_bit(uargp, i);
		} else if (!strcmp(argv[1], "none")) {
			for (i = 0; i < 64; i++)
				unset_bit(uargp, i);
		} else {
		#ifdef EXTRA_CREDIT
			str = malloc(2 * sizeof(char));
			strncpy(str, argv[1], 2);
			if (!strcmp(str, "0x"))
				hex_from_user = strtoul(argv[1], NULL, 0);
			else {
				err = ioctl(fd, GET_BITMAP, kargp);
				if (err == -1) {
					printf("ioctl call failed errno : %d\n",
							 errno);
					goto out;
				}
				set_bitmap(kargp, argv[1]);
				uargp = kargp;
			}

		#else
			hex_from_user = strtoul(argv[1], NULL, 0);
		#endif
		}

		err = ioctl(fd, SET_BITMAP, uargp);
		if (err == -1) {
			printf("ioctl call failed errno : %d\n", errno);
			goto out;
		}
	} else {
	#ifdef EXTRA_CREDIT
		err = ioctl(fd, GET_BITMAP, kargp);
		if (err == -1) {
			printf("ioctl call failed errno : %d\n", errno);
			goto out;
		}
		for (i = 1; i < argc - 1; i++)
			set_bitmap(kargp, argv[i]);
		uargp = kargp;
		err = ioctl(fd, SET_BITMAP, uargp);
		if (err == -1) {
			printf("ioctl call failed errno : %d\n", errno);
			goto out;
		}
	#else
		printf("Incorrect no.of arguments\n");
	#endif
	}
out:
	close(fd);
	return err;
}

