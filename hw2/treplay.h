#ifndef HEADER_H
#define HEADER_H

#include <unistd.h>
#include <stdint.h>

struct mkdir_record {
	uint16_t size;
	uint8_t type;
	uint32_t id;
	int32_t ret_val;
	uint32_t mode;
	uint16_t len_pathname;
	char pathname[1];
};

/* rmdir record structure */
struct rmdir_record {
	uint16_t size;
	uint8_t type;
	uint32_t id;
	int32_t ret_val;
	uint16_t len_pathname;
	char pathname[1];
};

struct record_open {
	uint16_t size;
	uint8_t type;
	uint32_t id;
	int32_t ret_val;
	uint32_t flags;
	uint32_t mode;
	unsigned long pid;
	unsigned long file_ptr;
	uint16_t len_pathname;
	char pathname[1];
};

struct record_flush {
	uint16_t size;
	uint8_t type;
	uint32_t id;
	int32_t ret_val;
	unsigned long pid;
	unsigned long file_ptr;
	uint16_t len_pathname;
	char pathname[1];
};
struct record_release {
        uint16_t size;
	uint8_t type;
        uint32_t id;
        int32_t ret_val;
	unsigned long pid;
	unsigned long file_ptr;
	uint16_t len_pathname;
	char pathname[1];
};



/* write record structure */
struct record_write {
	uint16_t size;
	uint8_t type;
	uint32_t id;
	int32_t ret_val;
	uint32_t flags;
	uint32_t mode;
	unsigned long pid;
	unsigned long file_ptr;
	uint16_t len_pathname;
	uint32_t buf_len;
	char pathname[1];

};

/* read record structure */
struct record_read {
	uint16_t size;
	uint8_t type;
	uint32_t id;
	int32_t ret_val;
	uint32_t flags;
	uint32_t mode;
	unsigned long pid;
	unsigned long file_ptr;
	uint16_t len_pathname;
	uint32_t buf_len;
	char pathname[1];
};

/* link record structure */
struct record_link {
	uint16_t size;
	uint8_t type;
	uint32_t id;
	int32_t ret_val;
	uint16_t len_old_pathname;
	uint16_t len_new_pathname;
	char pathname[1];
};
/* rename record structure */
struct record_rename {
	uint16_t size;
	uint8_t type;
	uint32_t id;
	int32_t ret_val;
	uint16_t len_old_pathname;
	uint16_t len_new_pathname;
	char pathname[1];

};

/* unlink record structure */
struct record_unlink {
	uint16_t size;
	uint8_t type;
	uint32_t id;
	int32_t ret_val;
	uint16_t len_pathname;
	char pathname[1];
};

/* symlink record structure */
struct record_symlink {
	uint16_t size;
	uint8_t type;
	uint32_t id;
	int32_t ret_val;
	uint16_t len_old_pathname;
	uint16_t len_new_pathname;
	char pathname[1];
};

int traceplay(char *filename, int n, int s);
int replay_syscall(void *rec, char *pathname, uint8_t type, int n, int s);
int replay_syscall_read_write(void *rec, char *pathname, char *buffer,
			uint8_t type, int n, int s);
void display_record(void *rec, char *pathname, uint8_t type);
int replay_syscall_multiple(void *rec, char *old_pathname, char *new_pathname,
		uint8_t type, int n, int s);
void display_record_multiple(void *rec, char *pathname, char *new_pathname,
		uint8_t type);


#endif
